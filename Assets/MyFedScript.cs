﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class MyFedScript : MonoBehaviour {

	public Text FedNameText;
	public Text FedBonusText;
	public Image FedImage;

	public Text CommishNameText;
	public Text CommishBonusText;
	public Image CommishImage;

	public Mask ratings;

	private float ratingsWidth;
	private GameObject UIManager;
	private GameObject HUD;

	public void Start(){
	}
	// Use this for initialization
	void OnEnable () {
		if(ratingsWidth == 0){
			ratingsWidth = ratings.rectTransform.rect.width;
		}

		UIManager = GameObject.Find("UIManager");
		HUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;
		HUD.SetActive(false);

		ratings.rectTransform.sizeDelta = new Vector2(ratingsWidth * GamePlaySession.instance.averageRating,
		                                              ratings.rectTransform.rect.height  );
		FedNameText.text = UIManager.GetComponent<GameSetupItems>().
			Federations[GamePlaySession.instance.selectedFederation].NameString;
		FedBonusText.text = UIManager.GetComponent<GameSetupItems>().
			Federations[GamePlaySession.instance.selectedFederation].BonusString;
		FedImage.sprite = UIManager.GetComponent<GameSetupItems>().
			Federations[GamePlaySession.instance.selectedFederation].littleSprite;

		CommishNameText.text = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.selectedComissioner].NameString;
		CommishBonusText.text = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.selectedComissioner].BonusString;
		CommishImage.sprite = UIManager.GetComponent<GameSetupItems>().
			Commisioners[GamePlaySession.instance.selectedComissioner].littleSprite;

	}
	
	// Update is called once per frame
	void OnDisable() {
		HUD.SetActive(true);
	}
}
