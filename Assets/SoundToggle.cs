﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SoundToggle : MonoBehaviour {

	public Text musicToggle;
	public Text sfxToggle;

	public GameObject UIManager;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	public void MusicToggle(){
		if (musicToggle.text.Contains("ON")){
			musicToggle.text = "Music OFF!";
			UIManager.GetComponent<AudioSource>().enabled = false;
		}
		else{
			musicToggle.text = "Music ON!";
			UIManager.GetComponent<AudioSource>().enabled = true;
			UIManager.GetComponent<AudioSource>().Play();
		}
	}

	// Update is called once per frame
	public void SFXToggle(){
		if (sfxToggle.text.Contains("ON")){
			sfxToggle.text = "SFX OFF!";
			UIManager.GetComponent<UIManagerScript>().sfxEnabled = false;
		}
		else{
			sfxToggle.text = "SFX ON!";
			UIManager.GetComponent<UIManagerScript>().sfxEnabled  = true;
			//TODO: Play a click sound
		}
	}
}
