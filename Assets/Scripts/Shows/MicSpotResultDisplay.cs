﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MicSpotResultDisplay : ResultDisplay {
	
	public Text segmentTitle;
	public Image segmentIcon;
	public Image[] wrestlerHeadshots;
	public Text[] wrestlerSkills;
	public Image segmentRating;
	public Text segmentText;
	
	public override void UpdateResultData() {
		if (segment != null && segment.cards != null) {
			// Tell the segment to get the segment results!
			segment.GetResults();
			
			// Adapt result box height
			LayoutElement layout = this.gameObject.GetComponent<LayoutElement>();
			if (layout != null) {
				if (segment.team1.Length < 2) {
					layout.preferredHeight = 300;
				} else {
					layout.preferredHeight = 400;
				}
			}

			// Display the results!
			segmentTitle.text = segment.segmentTitle;
			
			for (int i = 0;i <= 4;i++) {
				if (segment.team1.Length > i) {
					if (segment.team1[i] != null) {
						wrestlerSkills[i].transform.parent.gameObject.SetActive(true);
						wrestlerHeadshots[i].sprite = segment.team1[i].headshotImage;
						if (segment.team1SkillChange[i] > 0) {
							wrestlerSkills[i].text = "+" + segment.team1SkillChange[i].ToString() + " MIC!";
						} else {
							wrestlerSkills[i].text = "";
						}
					} else {
						wrestlerSkills[i].transform.parent.gameObject.SetActive(false);
					}
				} else {
					wrestlerSkills[i].transform.parent.gameObject.SetActive(false);
				}
			}
			
			segmentRating.fillAmount = segment.GetStarRating();

			MicSpotCardObject micSpot = (MicSpotCardObject) segment.cards[0];
			
			if (micSpot != null) {
				if (segmentIcon != null) {
					segmentIcon.sprite = micSpot.headshotImage;
					
					Image iconBorder = segmentIcon.transform.parent.gameObject.GetComponent<Image>();
					if (iconBorder != null) {
						if (segment.segmentPassed) {
							iconBorder.color = Color.yellow;
						} else {
							iconBorder.color = Color.white;
						}
					}
				}

				string resultMessage = "";
				if (segment.segmentPassed) {
					resultMessage = micSpot.result.Pass;
				} else {
					resultMessage = micSpot.result.Fail;
				}
				
				// Parse the result text and replace (WRESTLER) text with names
				int textPos = -1;
				int lastPos = 0;
				for (int i = 0; i < segment.team1.Length; i++) {
					textPos = resultMessage.IndexOf("(WRESTLER)");
					if (textPos > -1) {
						// We have a (WRESTLER) tag to replace
						string name = segment.team1[i].name.Trim();
						resultMessage = resultMessage.Remove(textPos, 10);
						resultMessage = resultMessage.Insert(textPos, name);
						lastPos = textPos + name.Length;
					} else {
						// We have no more (WRESTLER) tags so append to the end of the last name
						string name = "";
						if (i == (segment.team1.Length - 1)) {
							// This is the last name, so we should add "and" rather than a comma
							name = " and " + segment.team1[i].name.Trim();
						} else {
							// This isn't the last name, so we should use a comma
							name = ", " + segment.team1[i].name.Trim();
						}
						resultMessage = resultMessage.Insert(lastPos, name);
						lastPos += name.Length;
					}
				}
				
				segmentText.text = resultMessage;
				
				foreach (Image headshot in wrestlerHeadshots) {
					Image border = headshot.transform.parent.gameObject.GetComponent<Image>();
					if (border != null) {
						if (segment.segmentPassed) {
							border.color = Color.yellow;
						} else {
							border.color = Color.white;
						}
					}
				}
			} else {
				segmentText.text = "";
			}
		}
	}

}
