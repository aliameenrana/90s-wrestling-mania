﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MatchResultDisplay : ResultDisplay {

	public Text segmentTitle;
	public Image wrestlerHeadshot1;
	public Image wrestlerHeadshot2;
	public Image wrestlerBorder1;
	public Image wrestlerBorder2;
	public Text wrestlerWinner1;
	public Text wrestlerWinner2;
	public Text wrestlerSkill1;
	public Text wrestlerSkill2;
	public Image segmentRating;
	public Text segmentText;

	public override void UpdateResultData() {
		if (segment != null && segment.cards != null) {
			// Tell the segment to get the segment results!
			segment.GetResults();

			// Display the results!
			segmentTitle.text = segment.segmentTitle;

			wrestlerHeadshot1.sprite = GamePlaySession.instance.CheckCard(segment.team1[0].CardNumber).headshotImage;
			wrestlerHeadshot2.sprite = GamePlaySession.instance.CheckCard(segment.team2[0].CardNumber).headshotImage;

			if (segment.winners[0].CardNumber == segment.team1[0].CardNumber) {
				wrestlerWinner1.text = "Winner!";
				wrestlerWinner2.text = "";
			} else {
				wrestlerWinner1.text = "";
				wrestlerWinner2.text = "Winner!";
			}

			if (segment.team1SkillChange[0] > 0) {
				wrestlerSkill1.text = "+" + segment.team1SkillChange[0].ToString() + " SKILL!";
			} else {
				wrestlerSkill1.text = "";
			}

			if (segment.team2SkillChange[0] > 0) {
				wrestlerSkill2.text = "+" + segment.team2SkillChange[0].ToString() + " SKILL!";
			} else {
				wrestlerSkill2.text = "";
			}

			segmentRating.fillAmount = segment.GetStarRating();

			segmentText.text = segment.winners[Random.Range(0,segment.winners.Length-1)].victoryLine;

			if (segment.winners[0].CardNumber == segment.team1[0].CardNumber) {
				wrestlerBorder1.color = Color.yellow;
				wrestlerBorder2.color = Color.white;
				wrestlerWinner1.color = Color.yellow;
				wrestlerWinner2.color = Color.white;
				wrestlerSkill1.color = Color.yellow;
				wrestlerSkill2.color = Color.white;
			} else {
				wrestlerBorder1.color = Color.white;
				wrestlerBorder2.color = Color.yellow;
				wrestlerWinner1.color = Color.white;
				wrestlerWinner2.color = Color.yellow;
				wrestlerSkill1.color = Color.white;
				wrestlerSkill2.color = Color.yellow;
			}
		}
	}
}
