﻿using UnityEngine;
using System.Collections;

public class ResultDisplay : MonoBehaviour {

	public ShowObject show;
	public SegmentObject segment;

	public virtual void UpdateResultData() {}
}
