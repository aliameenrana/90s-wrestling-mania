﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CalendarScreen : MonoBehaviour {
	public WeekDisplay[] weekDisplays;

	public DatePanel datePanel;

	public Animator shopScreen;

	public Color completeShowColor;
	public Color nextShowColor;
	public Color lockedShowColor;

	public Button shopButton;

	public PopUpMessage initialPopUp;
	
	public int flavorTutorialWeek;
	public PopUpMessage flavorTutorialMessage;

	public int resultTutorialWeek;
	public PopUpMessage resultTutorialMessage;

	public int storeTutorialWeek;
	public PopUpMessage[] giftMessages;
	public PopUpMessage continueShopTutorialMessage;

	public GameObject UIManager;
	public GameObject playerHUD;

	private int lastCash;
	private int lastTokens;

	void Update() {
		if (GamePlaySession.instance == null)
			return;

		//CheckTutorialPopups();

		if (shopButton != null) {
			if (GamePlaySession.instance.storePurchaseTutorialClear || GamePlaySession.instance.newGamePlus) {
				shopButton.interactable = true;
			} else {
				shopButton.interactable = false;
			}
		}

		if (playerHUD != null && playerHUD.activeInHierarchy) {
			if (GamePlaySession.instance.myCash != lastCash || GamePlaySession.instance.myTokens != lastTokens) {
				UIManager.GetComponent<UIManagerScript>().UpdateHUD();
				lastCash = GamePlaySession.instance.myCash;
				lastTokens = GamePlaySession.instance.myTokens;
			}
		}
	}

	void CheckTutorialPopups() {
		if (!GamePlaySession.instance.inTutorialMode)
			return;

		if (GamePlaySession.instance.myShows.Count >= flavorTutorialWeek && GamePlaySession.instance.myShows[flavorTutorialWeek - 1].showFinished) {
			if ((!GamePlaySession.instance.flavorTutorialClear && !GamePlaySession.instance.newGamePlus) || GamePlaySession.instance.CheckCard(199) == null) {
				if (flavorTutorialMessage != null && !string.IsNullOrEmpty (flavorTutorialMessage.message)) {
					PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
					
					if (popUpWindow != null) {
						popUpWindow.OpenPopUpWindow (new PopUpMessage[] {flavorTutorialMessage}, null);
					}
				}
				
				GamePlaySession.instance.flavorTutorialClear = true;
				return;
			}
		}

		if (GamePlaySession.instance.myShows.Count >= resultTutorialWeek && GamePlaySession.instance.myShows[resultTutorialWeek - 1].showFinished) {
			if (!GamePlaySession.instance.resultTutorialClear && !GamePlaySession.instance.newGamePlus) {
				if (resultTutorialMessage != null && !string.IsNullOrEmpty(resultTutorialMessage.message)) {
					PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
					
					if (popUpWindow != null && shopScreen != null) {
						popUpWindow.OpenPopUpWindow(new PopUpMessage[] {resultTutorialMessage}, this.GetComponentInParent<Animator>());
					}
				}

				GamePlaySession.instance.resultTutorialClear = true;
				return;
			}
		}

		if (GamePlaySession.instance.myShows.Count >= storeTutorialWeek && GamePlaySession.instance.myShows[storeTutorialWeek - 1].showFinished) {
			if (!GamePlaySession.instance.newGamePlus) {
				if (!GamePlaySession.instance.storePurchaseTutorialClear) {
					if (playerHUD != null)
						playerHUD.SetActive(false);

					if (giftMessages.Length > 0) {
						PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
						
						if (popUpWindow != null && shopScreen != null) {
							popUpWindow.OpenPopUpWindow(giftMessages, shopScreen);
						}
					}

					GamePlaySession.instance.storePurchaseTutorialClear = true;
					return;
				} else if (GamePlaySession.instance.storePurchaseTutorialClear && !GamePlaySession.instance.storeTutorialClear) {
					if (playerHUD != null)
						playerHUD.SetActive(false);

					PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
					
					if (popUpWindow != null && shopScreen != null) {
						popUpWindow.OpenPopUpWindow(new PopUpMessage[] {continueShopTutorialMessage}, shopScreen);
					}
				}
			}
		}
	}

	public void OpenCalendar() {
		
		if (playerHUD != null)
			playerHUD.SetActive(true);
		
		UpdateDatePanel();
		CheckWeekButtons();

		if (ScreenHandler.current != null)
			ScreenHandler.current.OpenPanel(this.gameObject.GetComponent<Animator>());

		CheckTutorialPopups();
	}

	public void UpdateDatePanel() {
		if (datePanel == null)
			return;

		datePanel.UpdateDateDisplay();
	}
	
	public void CheckWeekButtons() {
		for(int i = 0;i < weekDisplays.Length;i++) {
			WeekItem currentWeek = GamePlaySession.instance.GetWeekInCurrentMonth(i);

			if (currentWeek != null) {
				weekDisplays[i].week = currentWeek;

				int thisShowID = currentWeek.showID;

				// Get the current week variable and images
				ShowObject thisWeek = null;
				if (GamePlaySession.instance.myShows.Count > thisShowID) {
					thisWeek = GamePlaySession.instance.myShows[thisShowID];
				}

				Image borderImage = weekDisplays[i].gameObject.GetComponent<Image>();
				Image iconImage = weekDisplays[i].showIcon;

				if (thisShowID == 0) {
					// We're the first show, so default to active and check if we've completed it yet
					weekDisplays[i].weekButton.interactable = true;

					if (thisWeek != null && thisWeek.showFinished) {
						// Show is finished
						borderImage.color = completeShowColor;
						iconImage.color = completeShowColor;
					} else {
						// Show isn't yet finished
						borderImage.color = nextShowColor;
						iconImage.color = nextShowColor;
					}
				} else {
					// We're not the first show, so check last week's show for completion
					int lastShowID = thisShowID - 1;

					if (lastShowID >= GamePlaySession.instance.myShows.Count) {
						// If there's no show yet for the previous week, lock this week
						weekDisplays[i].weekButton.interactable = false;

						borderImage.color = lockedShowColor;
						iconImage.color = lockedShowColor;
					} else {
						// Check if previous week's show is finished
						ShowObject lastWeek = GamePlaySession.instance.myShows[lastShowID];
						if (lastWeek.showFinished) {
							weekDisplays[i].weekButton.interactable = true;

							// Is there a show entry for this week?
							if (thisWeek != null && thisWeek.showFinished) {
								// Show is finished
								borderImage.color = completeShowColor;
								iconImage.color = completeShowColor;
							} else {
								// Show isn't yet finished
								borderImage.color = nextShowColor;
								iconImage.color = nextShowColor;
							}
						} else {
							// Last week's show isn't finished yet
							weekDisplays[i].weekButton.interactable = false;

							borderImage.color = lockedShowColor;
							iconImage.color = lockedShowColor;
						}
					}
				}

				// Update this week's display
				weekDisplays[i].UpdateShowDisplay();
			}
		}
	}
}
