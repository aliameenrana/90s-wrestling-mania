﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TagMatchSegmentSetup : SegmentSetup {
	
	public override bool CheckReady() {
		bool isReady = true;
		
		if (cards[0] == null)
			isReady = false;
		
		if (cards[2] == null)
			isReady = false;
		
		if (cards[3] == null)
			isReady = false;
		
		if (cards[5] == null)
			isReady = false;
		
		if (cards[6] == null)
			isReady = false;
		
		return isReady;
	}
	
	public override void SetFlavorButtons(List<FlavorCardObject> flavors) {
		int[] flavorOptions = new int[] {1, 4, 7};
		foreach (int option in flavorOptions) {
			Button button = optionButtons[option].GetComponentInChildren<Button>();
			if (button != null) {
				bool hasFlavor = false;
				
				CardSelector selector = selectors[option];
				
				foreach (FlavorCardObject flavor in flavors) {
					if (CheckCardPlayable(flavor, selector)) {
						hasFlavor = true;
					}
				}
				
				if (hasFlavor) {
					button.interactable = true;
				} else {
					button.interactable = false;
				}
			}
		}
	}
	
	public override void SetTitleCards(SegmentObject segment) {
		if (!segment.isTitleMatch)
			return;

		if (segment.titleMatchType != SegmentObject.TitleMatchType.TAG)
			return;

		Debug.Log("We are setting the cards to the champions!");
		
		segment.cards[2] = GamePlaySession.instance.tagChampions[0];
		segment.cards[3] = GamePlaySession.instance.tagChampions[1];
	}

	void OnEnable(){
		//if (GameObject.Find("UIManager").GetComponent<UIManagerScript>().sfxEnabled)
			//audio.Play();
	}
	
	public override void CheckCardCompatibility(int index) {
		SegmentObject segment = GamePlaySession.instance.myShows[segmentDisplay.showSetup.currentWeek.showID].segments[segmentDisplay.segmentID];
		
		int wrestler1Index = 0;
		int wrestler2Index = 0;
		int flavorIndex = 0;
		
		if (index == 2 || index == 5) {
			// Wrestler #1 card
			wrestler1Index = index;
			wrestler2Index = index + 1;
			flavorIndex = index + 2;
		} else if (index == 3 || index == 6) {
			// Wrestler #2 card
			wrestler1Index = index - 1;
			wrestler2Index = index;
			flavorIndex = index + 1;
		} else if (index == 4 || index == 7) {
			// Flavor card
			wrestler1Index = index - 2;
			wrestler2Index = index - 1;
			flavorIndex = index;
		}
		
		if ((wrestler1Index > 0 || wrestler2Index > 0) && flavorIndex > 0) {
			if (cards[flavorIndex] != null) {
				WrestlerCardObject[] wrestlers = new WrestlerCardObject[2];

				if (cards[wrestler1Index] != null)
					wrestlers[0] = (WrestlerCardObject) cards[wrestler1Index];
				if (cards[wrestler2Index] != null)
					wrestlers[1] = (WrestlerCardObject) cards[wrestler2Index];

				FlavorCardObject flavor = (FlavorCardObject) cards[flavorIndex];

				if (wrestlers.Length > 0 && flavor.cardClass == "Manager") {
					// We have a manager card selected, so let's make sure it is compatible in alignment
					bool mismatch = true;
					
					foreach (WrestlerCardObject wrestler in wrestlers) {
						if (wrestler != null) {
							if (wrestler.cardAlignment.ToUpper() == flavor.characterAlignment.ToUpper()) {
								// The alignment of the cards is a mismatch
								mismatch = false;
							}
						}
					}

					if (mismatch) {
						// We had a mismatch so deselect the other card
						if (index != flavorIndex) {
							cards[flavorIndex] = null;
						} else {
							cards[wrestler1Index] = null;
							cards[wrestler2Index] = null;
						}
						SetData(segmentDisplay);
					}
				}
			}
		}
	}
}
