﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardViewBackVenue : CardViewBack {

	public Text cardTitle;
	public Image headshotSprite;
	public Text flavorText;
	public Text costText;
	public Text capacityText;
	public Text usageType;
	
	public override void UpdateBackInfo(CardObject card) {
		// Set card back
		Image cardBackImage = transform.parent.gameObject.GetComponent<Image>();
		cardBackImage.sprite = regCardBackSprite;

		cardTitle.text = card.name;
		headshotSprite.sprite = card.headshotImage;

		VenueCardObject co = (VenueCardObject) card;

		if(co.costToPlay == 0 ){
			costText.text = "FREE";
		}
		else{
			costText.text = "$"+co.costToPlay;
		}
		capacityText.text = ""+co.capacity;
		flavorText.text = co.flavorText;
		if(co.venueLimit == "REG"){
			usageType.text = "Non-P4Vs";
		}
		else if(co.venueLimit == "REG"){
			usageType.text = "P4V";
		}
		else{
			usageType.text = "All";
		}
	}
}
