﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardViewBackWrestler : CardViewBack {

	public Image logoSprite;
	public Image headshotSprite;

	public Text statSkill;
	public Text statPush;
	public Text statMic;
	public Text statPop;
	
	public Text statusData;
	public Text styleData;

	public Text fanData;
	public Text otherStatsData;
	public Text cardDescription;

	public LayoutElement beltImages;
	public Image worldBeltImage;
	public Image titleBeltImage;
	public Image tagBeltImage;

	public Sprite worldChampionSprite;
	public Sprite titleChampionSprite;
	public Sprite tagChampionSprite;

	GameSetupItems items;
	
	public override void UpdateBackInfo(CardObject card) {

		WrestlerCardObject wrestler = (WrestlerCardObject) card;

		// Check if card is foil
		Image cardBackImage = transform.parent.gameObject.GetComponent<Image>();
		if (wrestler.cardVersion == "FOIL") {
			cardBackImage.sprite = foilCardBackSprite;
		} else {
			cardBackImage.sprite = regCardBackSprite;
		}

		logoSprite.sprite = wrestler.logoImage;
		headshotSprite.sprite = wrestler.headshotImage;

		statusData.text = wrestler.characterStatus;

		styleData.text = wrestler.wrestlerType;

		fanData.text = wrestler.cardAlignment;
		if (wrestler.cardAlignment == "Good") {
			fanData.color = goodColor;
		} else {
			fanData.color = badColor;
		}

		statSkill.text = wrestler.skillCurrent.ToString();
		statMic.text = wrestler.micCurrent.ToString();
		statPop.text = wrestler.popCurrent.ToString();
		statPush.text = wrestler.pushCurrent.ToString();

		otherStatsData.text = wrestler.characterHeight + " " + wrestler.characterWeight + " " + wrestler.characterHometown;
		cardDescription.text = wrestler.flavorText;

		if (beltImages != null) {
			bool worldChamp = false;
			bool titleChamp = false;
			bool tagChamp = false;

			if (GamePlaySession.instance != null) {
				if (GamePlaySession.instance.worldChampion != null && GamePlaySession.instance.worldChampion.CardNumber == wrestler.CardNumber) {
					worldChamp = true;
				}
				if (GamePlaySession.instance.titleChampion != null && GamePlaySession.instance.titleChampion.CardNumber == wrestler.CardNumber) {
					titleChamp = true;
				}
				if (GamePlaySession.instance.tagChampions != null) {
					foreach (WrestlerCardObject champ in GamePlaySession.instance.tagChampions) {
						if(champ != null){
							Debug.Log("Champ = " + champ.name);
							if (champ.CardNumber == wrestler.CardNumber) {
								tagChamp = true;
							}
						}
					}
				}

				if (worldChamp || titleChamp || tagChamp) {
					beltImages.preferredHeight = 40;
					statusData.text = "CHAMP";
					if (worldChamp) {
						beltImages.preferredHeight += 20;
						worldBeltImage.gameObject.SetActive(true);
					} else {
						worldBeltImage.gameObject.SetActive(false);
					}
					if (titleChamp) {
						beltImages.preferredHeight += 20;
						titleBeltImage.gameObject.SetActive(true);
					} else {
						titleBeltImage.gameObject.SetActive(false);
					}
					if (tagChamp) {
						beltImages.preferredHeight += 20;
						tagBeltImage.gameObject.SetActive(true);
					} else {
						tagBeltImage.gameObject.SetActive(false);
					}
				} else {
					beltImages.preferredHeight = 0;
					worldBeltImage.gameObject.SetActive(false);
					titleBeltImage.gameObject.SetActive(false);
					tagBeltImage.gameObject.SetActive(false);
				}
			}
		}
	}

	public void SetAverageImage(Image img, float avg){
		img.fillAmount = avg;
	}
}
