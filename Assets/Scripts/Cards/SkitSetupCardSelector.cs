﻿using UnityEngine;
using System.Collections;

public class SkitSetupCardSelector : CardSelector {
	public SkitSegmentSetup skitSetup;
	
	public override void SelectCard(CardObject card) {
		skitSetup.UpdateOption(optionIndex, card);
	}
	
	public override CardObject GetCard () {
		return skitSetup.cards[optionIndex];
	}
	
	public override void ResetCard() {
		skitSetup.UpdateOption(optionIndex, null);
	}
}
