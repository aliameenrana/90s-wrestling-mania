﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DatePanel : MonoBehaviour {
	public Text yearText;
	public Text monthText;

	public void UpdateDateDisplay() {
		int year = GamePlaySession.instance.currentYear;
		int month = GamePlaySession.instance.currentMonth;

		if(year >= GamePlaySession.instance.myCalendar.years.Length){
			yearText.text = GamePlaySession.instance.myCalendar.years[0].yearName;
			monthText.text = GamePlaySession.instance.myCalendar.years[0].months[month].monthName;
		}
		else{
			yearText.text = GamePlaySession.instance.myCalendar.years[year].yearName;
			monthText.text = GamePlaySession.instance.myCalendar.years[year].months[month].monthName;
		}
	}

	public void NextMonth() {
		GamePlaySession.instance.currentMonth += 1;
		if (GamePlaySession.instance.currentMonth >= 12) {
			GamePlaySession.instance.currentMonth = 0;
			GamePlaySession.instance.currentYear += 1;

			if (GamePlaySession.instance.currentYear >= GamePlaySession.instance.numberOfYears) {
				GamePlaySession.instance.currentYear = 0;
			}
		}

		UpdateDateDisplay();
	}

	public void PrevMonth() {
		GamePlaySession.instance.currentMonth -= 1;

		if (GamePlaySession.instance.currentMonth < 0) {
			GamePlaySession.instance.currentMonth = 11;
			GamePlaySession.instance.currentYear -= 1;

			if (GamePlaySession.instance.currentYear < 0) {
				GamePlaySession.instance.currentYear = (GamePlaySession.instance.numberOfYears - 1);
			}
		}

		UpdateDateDisplay();
	}
}
