﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HelpScreen : MonoBehaviour {
	private GameObject UIManager;
	private GameObject playerHUD;
	
	public void CloseBack() {		
		UIManager = GameObject.Find("UIManager");
		UIManager.GetComponent<UIManagerScript>().GoToCalendar();
	}
	
	public void CloseHome() {
		UIManager = GameObject.Find("UIManager");
		UIManager.GetComponent<UIManagerScript>().HomeButton();
	}
}
