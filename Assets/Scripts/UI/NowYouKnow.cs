﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NowYouKnow : MonoBehaviour {
	public int cardChance;
	public int[] cardIDsAvailable;

	public Text nowYouKnowText;

	public CardSelectScreenScript cardViewer;

	private WeekItem weekData;

	private GameObject UIManager;
	private GameObject playerHUD;

	public void OpenNowYouKnowPanel(WeekItem week) {
		SetCurrentShow(week);

		UIManager = GameObject.Find("UIManager");
		playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;
		
		if (playerHUD != null)
			playerHUD.SetActive(false);
		
		if (ScreenHandler.current != null)
			ScreenHandler.current.OpenPanel(this.gameObject.GetComponent<Animator>());

		if (UIManager.GetComponent<UIManagerScript>().sfxEnabled)
			GetComponent<AudioSource>().Play();
	}
	
	public void SetCurrentShow(WeekItem week) {
		// Set new show data and wipe existing segments
		weekData = week;
		
		if (GamePlaySession.instance.myShows.Count <= weekData.showID) {
			GamePlaySession.instance.myShows.Insert(weekData.showID, weekData.GenerateShow());
		} else if (GamePlaySession.instance.myShows[weekData.showID] == null) {
			GamePlaySession.instance.myShows[weekData.showID] = weekData.GenerateShow();
		}

		UpdateDisplay();
	}

	public void UpdateDisplay() {
		if (nowYouKnowText != null) {
			if (GamePlaySession.instance.currentYear == 0 && GamePlaySession.instance.currentMonth == 0) {
				// We're in the tutorial week, so show the pre-defined Now You Know
				nowYouKnowText.text = "Every month “Now You Know!” segments will provide helpful hints to improve your 8MW experience!\n\nFor example: At the end of every single game year you’ll earn a bigger venue card, a better sponsor card, plus a free Token! Be sure to keep on playing to get awesome free stuff!";
			} else {
				GameSetupItems items = GameObject.Find("UIManager").GetComponent<GameSetupItems>();
				if (weekData.GetShow().showFinished) {
					nowYouKnowText.text = items.nowyouknows.blurbs[weekData.GetShow().blurbNumber];

				} else {
					int blurbNum = RollForBlurb(items.nowyouknows.blurbs);
					nowYouKnowText.text = items.nowyouknows.blurbs[blurbNum];
					weekData.GetShow().blurbNumber = blurbNum;
					GamePlaySession.instance.shownBlurbs.Add(blurbNum);
				}
			}
		}
	}

	int RollForBlurb(string[] blurbs) {
		int blurbNum = 0;
		
		List<int> blurbNums = new List<int>();
		
		for (int i = 0; i < blurbs.Length; i++) {
			if (!GamePlaySession.instance.shownBlurbs.Contains(i))
				blurbNums.Add(i);
		}
		
		if (blurbNums.Count > 0) {
			blurbNum = blurbNums[Random.Range(0, blurbNums.Count - 1)];
		} else {
			blurbNum = Random.Range(0, blurbs.Length - 1);
		}
		
		return blurbNum;
	}

	public void CloseNowYouKnowPanel() {
		GameSetupItems items = GameObject.Find("UIManager").GetComponent<GameSetupItems>();

		if (weekData.GetShow().showFinished) {
			GameObject.Find("UIManager").GetComponent<UIManagerScript>().GoToCalendar();
		} else {
			weekData.GetShow().showFinished = true;
			weekData.GetShow().showRating = 20f;

			// Check for unlocking a new card
			if (Random.Range(1, 100) <= cardChance && cardIDsAvailable.Length > 0) {
				int cardID = 0;
				int cardAttempt = 0;
				bool foundCardToGive = false;
				while (!foundCardToGive) {
					// Select a card from the pool of IDs and see if the player doesn't already own it
					cardID = cardIDsAvailable[Random.Range(0, cardIDsAvailable.Length - 1)];
					if (GamePlaySession.instance.CheckCard(cardID) == null) {
						foundCardToGive = true;
					}

					// Make sure we don't fall in to an infinite loop by giving a finite number of attempts
					cardAttempt += 1;
					if (cardAttempt >= 20)
						break;
				}

				// If we got a card to give to the player, let's process it
				if (foundCardToGive) {
					NewCardsObject newCards = new NewCardsObject();
					newCards.message = new PopUpMessage();
					newCards.message.message = "Congrats! You unlocked a new card!";

					newCards.cards.Add(items.GetCard(cardID).Clone());

					if (cardViewer != null)
						cardViewer.OpenCardView(newCards);
				} else {
					// Else just go back to the calendar
					GameObject.Find("UIManager").GetComponent<UIManagerScript>().GoToCalendar();
				}
			} else {
				GameObject.Find("UIManager").GetComponent<UIManagerScript>().GoToCalendar();
			}
		}
	}
}
