﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StarRatings : MonoBehaviour {
	public Image ratingsImage;
	public Text dudText;

	void Update() {
		if (ratingsImage.fillAmount == 0f) {
			dudText.gameObject.SetActive(true);
		} else {
			dudText.gameObject.SetActive(false);
		}
	}
}
