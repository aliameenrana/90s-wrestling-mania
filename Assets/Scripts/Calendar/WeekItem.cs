﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WeekItem {
	public int weekNum;
	public string weekName;

	public int showID;  // ID of the entry in myShows array.  Serialized
	public int showTypeID; // ID of the show type template to use

	public ShowObject GenerateShow() {
		return GameObject.Find("UIManager").GetComponent<GameSetupItems>().ShowTypes[showTypeID].Clone();
	}

	public ShowObject GetShow() {
		ShowObject show;
		
		if (GamePlaySession.instance != null && showID < GamePlaySession.instance.myShows.Count) {
			show = GamePlaySession.instance.myShows[showID];
		} else {
			show = GenerateShow();
		}

		return show;
	}

	public WeekItem(){

	}
}