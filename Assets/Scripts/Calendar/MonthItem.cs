﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MonthItem {
	public int monthNum;
	public string monthName;

	public WeekItem[] weeks;

	public MonthItem(){
	}
}
