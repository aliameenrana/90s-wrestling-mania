﻿using UnityEngine;
using System.Collections;

public class Transition2DScript : MonoBehaviour {

	public GameObject frontOfCard;
	public GameObject backOfCard;
	public float flipSpeed = 30f;

	// Use this for initialization
	void Start () {
		backOfCard.transform.localRotation = Quaternion.AngleAxis(180, Vector3.up);
		backOfCard.SetActive(false);
	}

	public void FlipCard(){
		StartCoroutine (Flip());

	}

	IEnumerator Flip(){
		float currentAngle = 0;
		while (true){
			frontOfCard.transform.RotateAround(this.transform.position, Vector3.up, flipSpeed * Time.fixedDeltaTime);
			backOfCard.transform.RotateAround(this.transform.position, Vector3.up, flipSpeed * Time.fixedDeltaTime);
			currentAngle += flipSpeed *Time.fixedDeltaTime;

			yield return null;
			if (currentAngle > 90){
				break;
			}
		}

		backOfCard.SetActive (!backOfCard.activeSelf);

		currentAngle = 0;
		while (true){
			frontOfCard.transform.RotateAround(this.transform.position, Vector3.up, flipSpeed * Time.fixedDeltaTime);
			backOfCard.transform.RotateAround(this.transform.position, Vector3.up, flipSpeed * Time.fixedDeltaTime);
			currentAngle += flipSpeed *Time.fixedDeltaTime;

			yield return null;
			if (currentAngle > 90){
				break;
			}
		}
	

	}
}
