﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class ExportAssetBundles {

	//Creating Asset Bundles For web Player
	[MenuItem ("Assets/Build AssetBundle Factory Area1")]
	static void CreateSceneBundle1(){
		
		// list of the levels to be streamed 
		string[] levels = new string[1]{"Assets/Scenes/Main.unity"};
		
		var path = EditorUtility.SaveFilePanel ("Save Resource", "", "8mwSceneIOS", "unity3d");
		
		if (path.Length != 0)
		{
			BuildPipeline.BuildStreamedSceneAssetBundle( levels,path, BuildTarget.iOS);
		}
	}

	//Creating Asset Bundles For web Player
	[MenuItem ("Assets/Build AssetBundle For Android")]
	static void CreateSceneBundleAndroid(){
		
		// list of the levels to be streamed 
		string[] levels = new string[1]{"Assets/Scenes/Main.unity"};
		
		var path = EditorUtility.SaveFilePanel ("Save Resource", "", "8mwSceneAndroid", "unity3d");
		
		if (path.Length != 0)
		{
			BuildPipeline.BuildStreamedSceneAssetBundle( levels,path, BuildTarget.Android);
		}
	}
}
