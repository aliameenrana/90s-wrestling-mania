﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BlinkerScript : MonoBehaviour {

	public float blinkTime;

	// Use this for initialization
	void Start () {
		StartCoroutine (Blink ());
	}

	IEnumerator Blink(){
		while (true) {
			yield return new WaitForSeconds(blinkTime);

			this.GetComponent<Graphic>().enabled = !this.GetComponent<Graphic>().enabled;
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
