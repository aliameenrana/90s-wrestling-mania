using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class NewCardsObject {
	public PopUpMessage message;
	public List<CardObject> cards;

	public NewCardsObject(){
		cards = new List<CardObject>();
	}
}

public class CardSelectScreenScript : MonoBehaviour {
	public enum CardFilter{
		WRESTLERS_ALL,
		VENUES,
		MATCHES,
		FLAVORS,
		MIC_SPOTS,
		SKITS,
		MANAGERS,
		SPONSORS,
		MERCH,
		TAG_TEAMS,
		FEUDS,
		ALL,
		WRESTLERS_GOOD,
		WRESTLERS_BAD,
		WRESTLERS_MAIN_EVENT,
		WRESTLERS_MIDCARDER,
		WRESTLERS_CURTAIN_JERKER
	}

	private bool isSelectingCard;
	public bool isCardSelect;

	public int maximumCards;

	public CardFilter currentFilter;
	public Text titleText;
	public Text filterText;
	public Text collectionText;

	public int currentCardIndex;

	public CardView cardViewer;
	public Animator cardViewerScreen;
	
	public GameObject collectionPanel;
	public GameObject selectButton;

	public GameObject notEnoughString;

	[HideInInspector]
	public Animator previousScreen;

	[HideInInspector]
	public bool hadHUD;

	private CardSelector selector;
	
	private GameObject UIManager;
	private GameObject playerHUD;

	private Queue<CardFilter> filterQueue = new Queue<CardFilter>();
	private List<CardObject> cardList = new List<CardObject>();
	private List<CardObject> cardLibrary = new List<CardObject>();

    private string[] alphabet = new string[26]
    {
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "g",
        "h",
        "i",
        "j",
        "k",
        "l",
        "m",
        "n",
        "o",
        "p",
        "q",
        "r",
        "s",
        "t",
        "u",
        "v",
        "w",
        "x",
        "y",
        "z"
    };

	public void GenerateFilters(CardFilter newFilter, List<CardType> exclude = null) 
	{
		bool isFound;
		currentFilter = newFilter;

		// Wipe queue if it already exists
		if (filterQueue.Count > 0)
			filterQueue.Clear();

		// Set up the queue of filters available
		filterQueue.Enqueue(currentFilter);
		foreach (CardFilter filter in Enum.GetValues(typeof(CardFilter))) 
		{
			if (filter == currentFilter)
				continue;

			isFound = false;
			if(exclude != null)
			foreach(CardType f in exclude)
			{
				if(filter.ToString().Contains(f.ToString()))
				{
					isFound = true;
					break;
				}
			}

			if(isFound)
				continue;

			filterQueue.Enqueue(filter);
		}

		currentCardIndex = 0;
	}

	public void OpenCardView(NewCardsObject newCards = null, Animator nextScreen = null, bool showHUD = false) {
		UIManager = GameObject.Find("UIManager");
		playerHUD = UIManager.GetComponent<UIManagerScript>().PlayerHUD;
		previousScreen = nextScreen;
		hadHUD = showHUD;
		if (playerHUD != null)
			playerHUD.SetActive(false);

		if (newCards != null) {
			cardLibrary = newCards.cards;
		
			foreach (CardObject card in newCards.cards) {
				if (GamePlaySession.instance.CheckCard(card.CardNumber) == null)
					GamePlaySession.instance.myCards.Add(card);
			}
			// Now reprocess segment based on new card list
			if (GamePlaySession.instance.currentSegment != null) {
				GamePlaySession.instance.currentSegment.ProcessCardIDs();
				if (nextScreen != null) {
					SegmentSetup setup = nextScreen.GetComponent<SegmentSetup>();
					if (setup != null) {
						setup.SetData(setup.segmentDisplay);
						setup.UpdateDisplay();
					}
				}
			}
		} else {
			cardLibrary = GamePlaySession.instance.myCards;
		}

		GenerateFilters(CardFilter.ALL);
		FilterCards();
		UpdateCollectionData();


		SetViewSelect(false);

		if (ScreenHandler.current != null && cardViewerScreen != null) {
			ScreenHandler.current.OpenPanel(cardViewerScreen);
		}
		if (newCards != null && newCards.message != null) {
			if (newCards.message.message != "") {
				PopUpWindow popUpWindow = UIManager.GetComponent<UIManagerScript>().popUpWindow;
				if (popUpWindow != null) {
					popUpWindow.OpenPopUpWindow(new PopUpMessage[] {newCards.message}, this.cardViewerScreen);
				}
			}
		}
	}

	public void OpenCardSelect(CardSelector caller) {
		// Set the selector and the previous screen
		selector = caller;
		previousScreen = selector.gameObject.GetComponentInParent<Animator>();

		// Refund any previously selected card's costs
		CardObject card = selector.GetCard();

		if (card != null) {
			GamePlaySession.instance.myCash += card.GetPlayCost();
		}

		// Establish and filter the card library
		cardLibrary = caller.cards;

		GenerateFilters(selector.filterType, selector.hideTypes);
		FilterCards();

		if (cardList.Count == 0){
			return;
		}

		SetViewSelect(true);
		
		if (ScreenHandler.current != null && cardViewerScreen != null) {
			ScreenHandler.current.OpenPanel(cardViewerScreen);
		}
		else{
			Debug.Log ("FAILURE!!!!");
		}
	}

	public void SetViewSelect(bool select) {
		isCardSelect = select;

		if (isCardSelect) {
			if (collectionPanel)
				collectionPanel.SetActive(false);
			if (selectButton)
				selectButton.SetActive(true);
			if (filterText && (GamePlaySession.instance.inTutorialMode || selector.filterType != CardFilter.WRESTLERS_ALL ))
				filterText.transform.parent.gameObject.SetActive(false);
			else
				filterText.transform.parent.gameObject.SetActive(true);
		} else {
			if (collectionPanel)
				collectionPanel.SetActive(true);
			if (selectButton){
				selectButton.SetActive(false);
			}
			if (filterText)
				filterText.transform.parent.gameObject.SetActive(true);
		}

		SetScreenTitle();
	}

	public void SetScreenTitle() {
		if (titleText) {
			if (isCardSelect) {
				titleText.text = GetCurrentFilterName();
			} else {
				titleText.text = "Cards";
			}
		}
	}

	public void ReturnToLastScreen() {
		if (hadHUD) {
			playerHUD = GameObject.Find("UIManager").GetComponent<UIManagerScript>().PlayerHUD;
			
			if (playerHUD != null)
				playerHUD.SetActive(true);
		}

		if (!isCardSelect && previousScreen == null) {
			UIManager = GameObject.Find("UIManager");
			UIManager.GetComponent<UIManagerScript>().GoToCalendar();
			return;
		}

		if (isCardSelect) {
			if (selector != null)
				selector.ResetCard();
		}

		if (ScreenHandler.current != null && previousScreen != null) {
			ScreenHandler.current.OpenPanel(previousScreen);
		}
	}
	public void SelectCard(CardView viewer) {
		StartCoroutine(SelectCardC(viewer));
	}
	public IEnumerator SelectCardC(CardView viewer){
		isSelectingCard = true;
		// Determine card cost
		int cost = viewer.myCard.GetPlayCost();

		// Check we can afford it or if it's free
		if (GamePlaySession.instance.myCash >= cost || cost <= 0) {
			// Subtract card cost
			GamePlaySession.instance.myCash -= cost;

			// Switch screens
			if (ScreenHandler.current != null && previousScreen != null) {
				ScreenHandler.current.OpenPanel(previousScreen);
			}
			else{
				Debug.Log ("FAILURE");
			}

			// Set the selector to select the card we chose
			yield return StartCoroutine(WaitForSelector(viewer));


		} else {
			yield return StartCoroutine(FlashMessage(notEnoughString));
		}
		isSelectingCard = false;
		yield return null;
	}

	public IEnumerator WaitForSelector(CardView viewer){
		while (selector == null)
			yield return null;
		selector.SelectCard(viewer.myCard);
	}
	public void UpdateCollectionData() {
		if (collectionText != null) {
			if (GamePlaySession.instance != null) {
				collectionText.text = "Collection: " + GamePlaySession.instance.myCards.Count.ToString() + "/" + maximumCards.ToString();
				collectionText.transform.parent.gameObject.SetActive(true);
			} else {
				collectionText.transform.parent.gameObject.SetActive(false);
			}
		}
	}

	public void SwitchCardFilter() {
		//if (isCardSelect)
		//	return;

		if (filterQueue.Count > 0) {
			CardFilter lastFilter = filterQueue.Dequeue();
			filterQueue.Enqueue(lastFilter);
			currentFilter = filterQueue.Peek();
			
			currentCardIndex = 0;
			FilterCards();
		}
	}
	public void OnDisable(){
		if(isSelectingCard){
			Debug.Log("I AM DISABLING While selecting card");
			UIManager = GameObject.Find("UIManager");
			UIManager.GetComponent<UIManagerScript>().GoToCalendar();
		}
	}
	public void FilterCards() 
	{
		List<CardObject> filteredCards = new List<CardObject>();
		if (cardLibrary.Count > 0) 
		{
			// Determine the current card type to check for, or simply return all cards.
			if (currentFilter == CardFilter.ALL) 
			{
				filteredCards = cardLibrary;
			} 
			else 
			{
				CardType[] filters = CheckFilterType();
				foreach (CardObject card in cardLibrary) 
				{
					foreach(CardType filter in filters) 
					{
						if (card.myCardType == filter)
						{
							if(filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_BAD)
							{
								WrestlerCardObject wrestler = (WrestlerCardObject) card;
								if(wrestler.cardAlignment == "Bad")
								{
									filteredCards.Add(card);
								}
							}
							else if(filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_GOOD)
							{
								WrestlerCardObject wrestler = (WrestlerCardObject) card;
								if(wrestler.cardAlignment == "Good")
								{
									filteredCards.Add(card);
								}
							}
							else if(filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_MAIN_EVENT)
							{
								WrestlerCardObject wrestler = (WrestlerCardObject) card;
								if(wrestler.characterStatus == "Main Eventer")
								{
									filteredCards.Add(card);
								}
							}
							else if(filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_MIDCARDER)
							{
								WrestlerCardObject wrestler = (WrestlerCardObject) card;
								if(wrestler.characterStatus == "Mid Carder")
								{
									filteredCards.Add(card);
								}
							}
							else if(filter == CardType.WRESTLER && currentFilter == CardFilter.WRESTLERS_CURTAIN_JERKER)
							{
								WrestlerCardObject wrestler = (WrestlerCardObject) card;
								if(wrestler.characterStatus == "Curtain Jerker")
								{
									filteredCards.Add(card);
								}
							}
							else
							{
								filteredCards.Add(card);
							}
						}
					}
				}
			}

			if (filteredCards.Count == 0) {
				// If there's no cards when using this filter, switch to the next one and try again, unless we're in card select mode
				if (!isCardSelect) {
					SwitchCardFilter();
					return;
				}
			}
		}
		cardList = filteredCards;
		// Sort the card list based on card number
		cardList.Sort((x, y) => string.Compare(x.name, y.name));

		string filterOutput = "Show: ";
		filterOutput += GetCurrentFilterName();
		filterText.text = filterOutput;

		ShowCurrentCard();
	}

	public string GetCurrentFilterName() {
		string name = "";

		switch (currentFilter) {
		case CardFilter.WRESTLERS_ALL:
			name = "Wrestlers All";
			break;
		case CardFilter.WRESTLERS_BAD:
			name = "Wrestlers Bad";
			break;
		case CardFilter.WRESTLERS_GOOD:
			name = "Wrestlers Good";
			break;
		case CardFilter.WRESTLERS_MAIN_EVENT:
			name = "Wrestlers Main Event";
			break;
		case CardFilter.WRESTLERS_CURTAIN_JERKER:
			name = "Wrestlers Curtain Jerker";
			break;
		case CardFilter.WRESTLERS_MIDCARDER:
			name = "Wrestlers Midcarder";
			break;
		case CardFilter.VENUES:
			name = "Venues";
			break;
		case CardFilter.MATCHES:
			name = "Matches";
			break;
		case CardFilter.FLAVORS:
			name = "Flavors";
			break;
		case CardFilter.MIC_SPOTS:
			name = "Mic Spots";
			break;
		case CardFilter.SKITS:
			name = "Skits";
			break;
		case CardFilter.MANAGERS:
			name = "Managers";
			break;
		case CardFilter.SPONSORS:
			name = "Sponsors";
			break;
		case CardFilter.MERCH:
			name = "Merch";
			break;
		case CardFilter.TAG_TEAMS:
			name = "Tag Teams";
			break;
		case CardFilter.FEUDS:
			name = "Feuds";
			break;
		default:
			name = "All";
			break;
		}

		return name;
	}

	public CardType[] CheckFilterType() {		
		switch (currentFilter) {
		case CardFilter.WRESTLERS_ALL:
		case CardFilter.WRESTLERS_BAD:
		case CardFilter.WRESTLERS_CURTAIN_JERKER:
		case CardFilter.WRESTLERS_GOOD:
		case CardFilter.WRESTLERS_MAIN_EVENT:
		case CardFilter.WRESTLERS_MIDCARDER:
			return new CardType[] {CardType.WRESTLER};
		case CardFilter.VENUES:
			return new CardType[] {CardType.VENUE};
		case CardFilter.MATCHES:
			return new CardType[] {CardType.MATCH};
		case CardFilter.FLAVORS:
			return new CardType[] {CardType.FLAVOR, CardType.MANAGER, CardType.SPONSOR, CardType.MERCH, CardType.TAG_TEAM, CardType.FEUD};
		case CardFilter.MIC_SPOTS:
			return new CardType[] {CardType.MIC_SPOT};
		case CardFilter.SKITS:
			return new CardType[] {CardType.SKIT};
		case CardFilter.MANAGERS:
			return new CardType[] {CardType.MANAGER};
		case CardFilter.SPONSORS:
			return new CardType[] {CardType.SPONSOR};
		case CardFilter.MERCH:
			return new CardType[] {CardType.MERCH};
		case CardFilter.TAG_TEAMS:
			return new CardType[] {CardType.TAG_TEAM};
		case CardFilter.FEUDS:
			return new CardType[] {CardType.FEUD};
		default:
			return new CardType[] {CardType.WRESTLER};
		}
	}

	public IEnumerator FlashMessage(GameObject messageString){
		messageString.SetActive(true);
		
		yield return new WaitForSeconds (3f);
		
		messageString.SetActive(false);
	}

	public void ShowCurrentCard(){
		if (cardList.Count > 0) {
			cardViewer.ShowCard(cardList[currentCardIndex]);
		}
	}

	public CardObject GetCurrentCard() {
		return cardList[currentCardIndex];
	}

	public void UserSwipeRight() {
		int modNum = cardList.Count;
		currentCardIndex--;
		
		if (currentCardIndex < 0) {
			currentCardIndex = modNum - 1;
		} else {
			currentCardIndex = currentCardIndex % modNum;
		}
		ShowCurrentCard();
	}

    public void Slide(Slider value)
    {
        var index = cardList.FindIndex(x => x.name.ToLower().StartsWith(alphabet[(int)value.value]));
        currentCardIndex = index >= 0 ? index : currentCardIndex;
        ShowCurrentCard();
    }
	
	public void UserSwipeLeft() {
		int modNum = cardList.Count;
		currentCardIndex++;
		currentCardIndex = currentCardIndex % modNum;
		
		ShowCurrentCard();
	}

}
