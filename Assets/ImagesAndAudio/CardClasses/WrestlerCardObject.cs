﻿using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;

[System.Serializable]
public class WrestlerCardObject : CardObject {

	[XmlElement("CharacterNumber")]
	public int characterNumber;

	[XmlElement("CharacterAlignment")]
	public string cardAlignment;

	[XmlElement("CardVersion")]
	public string cardVersion;

	[XmlElement("CharacterStatus")]
	public string characterStatus;

	[XmlElement("Rarity")]
	public string rarity;

	[XmlElement("WrestlerType")]
	public string wrestlerType;

	[XmlElement("Stats")]
	public Stats stats;

	[XmlElement("SkillCurrent")]
	public int skillCurrent;

	[XmlElement("MicCurrent")]
	public int micCurrent;

	[XmlElement("PopCurrent")]
	public int popCurrent;

	[XmlElement("PushCurrent")]
	public int pushCurrent;

	[XmlElement("CharacterHeight")]
	public string characterHeight;

	[XmlElement("CharacterWeight")]
	public string characterWeight;

	[XmlElement( "CharacterHometown")]
	public string characterHometown;

	[XmlElement("CharacterFinisher")]
	public string characterFinisher;

	[XmlElement("VictoryLine")]
	public string victoryLine;

	// Use this for initialization
	public WrestlerCardObject() : base(){
		myCardType = CardType.WRESTLER;
	}

	public override CardObject Clone(){
		myCardType = CardType.WRESTLER;
		byte[] wcoBA = this.ObjectToByteArray(this);
		WrestlerCardObject wco = (WrestlerCardObject)this.ByteArrayToObject(wcoBA);

		wco.skillCurrent = stats.SkillBase;
		wco.micCurrent = stats.MicBase;
		wco.popCurrent = stats.PopBase;
		wco.pushCurrent = stats.Push;

		foreach(BonusValue bonus in GamePlaySession.instance.myBonuses){
			if(bonus.bType == BonusValue.BonusType.MIC){
				wco.micCurrent += bonus.value;
			}
			else if(bonus.bType == BonusValue.BonusType.POP){
				wco.popCurrent += bonus.value;
			}
			else if(bonus.bType == BonusValue.BonusType.SKILL){
				wco.skillCurrent += bonus.value;
			}

		}

		int cardNum = (CardNumber % 2 == 1)?CardNumber:(CardNumber-1);
		wco.frontImage = GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
			//Resources.Load("Cards_Front/"+CardNumber.ToString("D3")+"_wrestler", typeof(Sprite)) as Sprite;
		wco.logoImage =  GamePlaySession.instance.GSI.cardLogoDict[cardNum];
			//Resources.Load("Cards_Back/"+cardNum.ToString("D3")+
		                                //"_logo", typeof(Sprite)) as Sprite;
		wco.headshotImage = GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
		//Resources.Load("Cards_Back/"+CardNumber.ToString("D3")+"_headshot", typeof(Sprite)) as Sprite;

		// Debug output for tracking down filenames, etc.
		/*Debug.Log("Card: " + wco.name);
		Debug.Log("Front Image = " + "Cards_Front/"+CardNumber.ToString("D3")+"_wrestler("+Regex.Replace(this.name,  @"[^\w\@-]", string.Empty)+")");
		Debug.Log("Logo Image = " + "Cards_Back/"+cardNum.ToString("D3")+"_logo("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");
		Debug.Log("Headshot Image = " + "Cards_Back/"+CardNumber.ToString("D3")+"_headshot("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");
*/
		return wco;
	}

	public override CardObject CloneWithProgress(CardObject obj)
	{

		myCardType = CardType.WRESTLER;
		byte[] wcoBA = this.ObjectToByteArray(this);
		WrestlerCardObject wco = (WrestlerCardObject)this.ByteArrayToObject(wcoBA);
		WrestlerCardObject current = (WrestlerCardObject)obj;
		
		wco.skillCurrent = current.skillCurrent;
		wco.micCurrent = current.micCurrent;
		wco.popCurrent = current.popCurrent;
		wco.pushCurrent = current.pushCurrent;
		
		int cardNum = (CardNumber % 2 == 1)?CardNumber:(CardNumber-1);
		wco.frontImage = GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
		wco.logoImage =  GamePlaySession.instance.GSI.cardLogoDict[cardNum];
		wco.headshotImage = GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];

		return wco;
	}

	public bool IncreaseMicRating(int segmentRating){
		int xp = Random.Range(1, 20);

		xp -= Mathf.Max(0 , this.micCurrent - segmentRating);

		if(xp > this.micCurrent){
			this.micCurrent += 1;
			this.micCurrent = Mathf.Min(this.micCurrent, this.stats.MicMax);
			return true;
		}
		return false;

	}

	public bool IncreasePopRating(int segmentRating){
		int xp = Random.Range(1, 20);
		
		xp -= Mathf.Max(0 , this.popCurrent - segmentRating);
		
		if(xp > this.popCurrent){
			this.popCurrent += 1;
			this.popCurrent = Mathf.Min(this.popCurrent, this.stats.PopMax);
			return true;
		}
		return false;
		
	}

	public bool IncreaseSkillRating(int segmentRating){
		int xp = Random.Range(1, 20);
		
		xp -= Mathf.Max(0 , this.skillCurrent - segmentRating);
		
		if(xp > this.skillCurrent){
			this.skillCurrent += 1;
			this.skillCurrent = Mathf.Min(this.skillCurrent, this.stats.SkillMax);
			return true;
		}
		return false;
	}

	public override void PostSerialize(){
		int cardNum = (CardNumber % 2 == 1)?CardNumber:(CardNumber-1);
		//this.frontImage = Resources.Load("Cards_Front/"+CardNumber.ToString("D3")+"_wrestler", typeof(Sprite)) as Sprite;
		//this.logoImage =  Resources.Load("Cards_Back/"+cardNum.ToString("D3")+"_logo", typeof(Sprite)) as Sprite;
		//this.headshotImage = Resources.Load("Cards_Back/"+CardNumber.ToString("D3")+"_headshot", typeof(Sprite)) as Sprite;
		this.frontImage = GamePlaySession.instance.GSI.cardFrontDict[CardNumber];
		this.logoImage =  GamePlaySession.instance.GSI.cardLogoDict[cardNum];
		this.headshotImage = GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];
	}

}

[System.Serializable]
public class Stats{
	[XmlElement("SkillBase")]
	public int SkillBase;

	[XmlElement("SkillMax")]
	public int SkillMax;

	[XmlElement( "MicBase")]
	public int MicBase;

	[XmlElement( "MicMax")]
	public int MicMax;

	[XmlElement("PopBase")]
	public int PopBase;

	[XmlElement("PopMax")]
	public int PopMax;

	[XmlElement("Push")]
	public int Push;

	public Stats(){

	}
}
