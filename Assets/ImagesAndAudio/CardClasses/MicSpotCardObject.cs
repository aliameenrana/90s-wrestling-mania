﻿using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

[System.Serializable]
public class MicSpotCardObject : CardObject {

	[XmlElement("Year")]
	public int yearAcquired;

	[XmlElement("Limit")]
	public int limit;
	
	[XmlElement("WrestlerMin")]
	public int wrestlerMin;

	[XmlElement("WrestlerMax")]
	public int wrestlerMax;
	
	[XmlElement("PassCheck")]
	public string passCheck;

	[XmlElement("SyncEffect")]
	public string syncEffect;
	
	[XmlElement("TypesBonus")]
	public TypesBonus bonuses;

	[XmlElement("Results")]
	public Results result;
	
	[System.NonSerialized]
	public bool lastCheckPassed;
	
	[XmlElement("UnlockRequirements")]
	public UnlockRequirements unlockRequirements;
	
	[XmlElement("FlavorCardType")]
	public string flavorCardType;

	[XmlElement("Alignment")]
	public string characterAlignment;
	
	// Use this for initialization
	public MicSpotCardObject() : base(){
		myCardType = CardType.MIC_SPOT;

	}
	
	public override CardObject Clone(){
		myCardType = CardType.MIC_SPOT;
		byte[] wcoBA = this.ObjectToByteArray(this);
		MicSpotCardObject fco = (MicSpotCardObject)this.ByteArrayToObject(wcoBA);
		
		//fco.frontImage = Resources.Load("Cards_Front/"+CardNumber.ToString("D3")+"_micspot", typeof(Sprite)) as Sprite;
		//fco.logoImage = Resources.Load("Cards_Back/"+CardNumber.ToString("D3")+"_logo", typeof(Sprite)) as Sprite;
		//fco.headshotImage = Resources.Load("Cards_Back/"+CardNumber.ToString("D3")+"_headshot", typeof(Sprite)) as Sprite;
		fco.frontImage = GamePlaySession.instance.GSI.cardFrontDict[CardNumber];//Resources.Load("Cards_Front/"+CardNumber.ToString("D3")+"_match", typeof(Sprite)) as Sprite;
		fco.headshotImage = GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];//Resources.Load("Cards_Back/"+CardNumber.ToString("D3")+"_headshot", typeof(Sprite)) as Sprite;
		fco.logoImage = GamePlaySession.instance.GSI.cardLogoDict[CardNumber];

		// Debug output for tracking down filenames, etc.
		/*Debug.Log("Card: " + fco.name);
		Debug.Log("Card Number: " +CardNumber);
		Debug.Log("Front Image = " + "Cards_Front/"+CardNumber.ToString("D3")+"_micspot("+Regex.Replace(this.name,  @"[^\w\@-]", string.Empty)+")");
		Debug.Log("Logo Image = " + "Cards_Back/"+CardNumber.ToString("D3")+"_logo("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");
		Debug.Log("Headshot Image = " + "Cards_Back/"+CardNumber.ToString("D3")+"_headshot("+Regex.Replace(this.name, @"[^\w\@-]", string.Empty)+")");
		*/
		return fco;
	}

	public int RateMicSpot(WrestlerCardObject[] wrestlers, FlavorCardObject flavorCard, out int[] wrestlerRatings, out List<ResultBonus> bonuses){
		int[] wRatings = new int[wrestlers.Length];
		bool skipCheck = false;
		
		bonuses = new List<ResultBonus>();

		float combined = 0f;
		
		wrestlerRatings = wRatings;
		
		for(int i = 0; i < wrestlers.Length; i++){
			if(wrestlers[i].wrestlerType == "Bigmouth" || wrestlers[i].wrestlerType == "Showman"){
				skipCheck = true;
			}
			wRatings[i] = Random.Range(1, 20);
			if(wRatings[i] <= wrestlers[i].micCurrent){
				wRatings[i] = wrestlers[i].micCurrent;
			}
			else{
				int penalty = (wrestlers[i].micCurrent - 1) - ((wRatings[i] - wrestlers[i].micCurrent)/3);
				wRatings[i] = penalty;
			}
			//TODO: At this point This is the base value for animated trasitions:  wRatings[i]
			wrestlerRatings[i] = wRatings[i];
			
			ResultBonus wrestlerBonus = new ResultBonus();
			wrestlerBonus.type = BonusType.Wrestler;
			wrestlerBonus.wrestlerNum = i;
			wrestlerBonus.cardNumber = wrestlers[i].CardNumber;

			if(wrestlers[i].wrestlerType == this.bonuses.TypeBonusA || wrestlers[i].wrestlerType == this.bonuses.TypeBonusB){
				skipCheck = true;
				wRatings[i] += 1;
				wrestlerBonus.value += 1;
			}
			
			ResultBonus flavorBonus = new ResultBonus();
			flavorBonus.type = BonusType.Flavor;
			flavorBonus.wrestlerNum = i;

			if(flavorCard != null){
				flavorBonus.cardNumber = flavorCard.CardNumber;
				if(flavorCard.bonuses.Mic > 0) {
					wRatings[i] += flavorCard.bonuses.Mic;
					flavorBonus.value += flavorCard.bonuses.Mic;
				}
			}
			
			if (wrestlerBonus.value > 0)
				bonuses.Add(wrestlerBonus);
			if (flavorBonus.value > 0)
				bonuses.Add(flavorBonus);

			//this function increase the skill rating based on a probability
			//inside the if statement you should handle displaying text if skill rating increased.
			if(wrestlers[i].IncreaseMicRating(wRatings[i])){

			}

			//TODO: Ikono this is animated transition after bonuses
			//the count should increase gradually in animation until it reaches this value: wRatings[i]

			combined += (float)wRatings[i];

			//if(flavorCard != null){
				//add the flavor card bonus to the rating
			//}
		}

		combined /= wRatings.Length;

		if(!this.GetPassCheck() && !skipCheck){
			lastCheckPassed = false;
			combined -= 4;
			if(combined <= 0){
				combined = 0;
			}
		} else {
			lastCheckPassed = true;
		}

		return (int) (combined);
	}
	
	public bool GetPassCheck(){
		if(this.passCheck == "EASY"){
			return true;
		}
		else if(this.passCheck == "MEDIUM" && Random.value <= .75f){
			return true;
		}
		else if(this.passCheck == "HARD" && Random.value <= .5f){
			return true;
		}
		return false;
	}

	public override void PostSerialize(){
		frontImage = GamePlaySession.instance.GSI.cardFrontDict[CardNumber];//Resources.Load("Cards_Front/"+CardNumber.ToString("D3")+"_match", typeof(Sprite)) as Sprite;
		headshotImage = GamePlaySession.instance.GSI.cardHeadshotDict[CardNumber];//Resources.Load("Cards_Back/"+CardNumber.ToString("D3")+"_headshot", typeof(Sprite)) as Sprite;
		logoImage = GamePlaySession.instance.GSI.cardLogoDict[CardNumber];
	}

}

[System.Serializable]
public class TypesBonus{
	public string TypeBonusA;
	public string TypeBonusB;
	public int MaxOut;

	public TypesBonus(){

	}
}

[System.Serializable]
public class Results{
	public string Pass;
	public string Fail;

	public Results(){

	}
}