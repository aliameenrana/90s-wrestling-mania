﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadGame : MonoBehaviour {

	public Text noticeText;
	public FlashText flash;
	int versionNumber;
	// Use this for initialization
	IEnumerator Start () {
		versionNumber = 9;
		// Download compressed scene. If version 5 of the file named "Streamed-Level1.unity3d" was previously downloaded and cached.
		// Then Unity will completely skip the download and load the decompressed scene directly from disk.
		#if UNITY_IPHONE
		var download = WWW.LoadFromCacheOrDownload ("http://52.91.182.229/8mw/8mwSceneIOS_"+versionNumber+".unity3d", versionNumber);
		#else
		var download = WWW.LoadFromCacheOrDownload ("http://52.91.182.229/8mw/8mwSceneAndroid_"+versionNumber+".unity3d", versionNumber);
		#endif
		
		yield return download;
		
		// Handle error
		if (download.error != null)
		{
			Debug.LogError(download.error);
			noticeText.text = download.error;
			flash.waiting = false;
            yield break;
			
		}
		
		// In order to make the scene available from LoadLevel, we have to load the asset bundle.
		// The AssetBundle class also lets you force unload all assets and file storage once it is no longer needed.
		AssetBundle bundle = download.assetBundle;
		
		//bundle.LoadAll ();
		
		download.Dispose();

		flash.waiting = false;
		Application.LoadLevel("Main");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
