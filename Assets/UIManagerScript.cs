﻿using UnityEngine;
using System.Collections;

public class UIManagerScript : MonoBehaviour
{

    public Animator TitleScreen;
    public Animator DisclaimerScreen;
    public Animator WelcomeScreen;
    public Animator FedScreen;
    public Animator FedScreenVerify;

    public Animator MyFedScreen;
    public Animator CommishSelectScreen;
    public Animator CommishScreenVerify;

    public Animator MainCalendarScreen;
    public Animator NowYouKnowScreen;
    public Animator ShowSetupScreen;
    public Animator MatchSetupScreen;
    public Animator TagMatchSetupScreen;
    public Animator MicSpotSetupScreen;
    public Animator SkitSetupScreen;
    public Animator ShowTransitionScreen;
    public Animator ShowResultsScreen;

    public Animator ShopScreen;

    public Animator HelpScreen;
    public Animator CreditsScreen;

    public EndGameScreen endGameScreen;

    public PopUpWindow popUpWindow;

    public CardSelectScreenScript cardViewer;

    public GameObject PlayerHUD;
    public bool sfxEnabled;

    public AudioSource buttonSoundSource;

    public GameObject screenNode;

    void Update()
    {
        if (screenNode != null && !screenNode.activeInHierarchy)
        {
            screenNode.SetActive(true);
            screenNode.transform.parent.gameObject.SetActive(true);

            if (ScreenHandler.current != null)
                ScreenHandler.current.RefreshPanel();
        }
    }

    public void NewGame()
    {
        GamePlaySession.instance = new GamePlaySession();
        GamePlaySession.instance.GSI = GetComponent<GameSetupItems>();
        // Add any code needed to flag the player as beginning a new game!
        if (ScreenHandler.current != null && DisclaimerScreen != null)
            ScreenHandler.current.OpenPanel(DisclaimerScreen);
    }

    public void NewGamePlus()
    {
        // Add any code needed to flag the player as beginning a new game+!
        GamePlaySession newGamePlus = new GamePlaySession();

        // Copy over cards, cash, and tokens
        newGamePlus.myCards = GamePlaySession.instance.myCards;
        newGamePlus.myCash = Mathf.Max(GamePlaySession.instance.myCash, 0);
        newGamePlus.myTokens = GamePlaySession.instance.myTokens;

        // Ensure the store doesn't reset itself
        newGamePlus.firstShopCards = true;
        newGamePlus.storeCards = GamePlaySession.instance.storeCards;
        newGamePlus.storeCardsBought = GamePlaySession.instance.storeCardsBought;
        newGamePlus.storePurchases = GamePlaySession.instance.storePurchases;
        newGamePlus.lastShopRefresh = GamePlaySession.instance.lastShopRefresh;

        // Set as a new game plus playthrough
        newGamePlus.newGamePlus = true;

        GamePlaySession.instance = newGamePlus;
        GamePlaySession.instance.GSI = GetComponent<GameSetupItems>();
        if (ScreenHandler.current != null && DisclaimerScreen != null)
            ScreenHandler.current.OpenPanel(DisclaimerScreen);
    }

    public void ContinueGame()
    {
        // Add any code needed to flag the player as continuing an existing game!
        //Update the HUD and turn it on where appropriate
        if (GamePlaySession.instance.selectedFederation == -1)
        {
            PlayerHUD.SetActive(true);
            if (ScreenHandler.current != null && FedScreen != null)
                ScreenHandler.current.OpenPanel(FedScreen);
            return;
        }

        if (GamePlaySession.instance.selectedComissioner == -1)
        {
            PlayerHUD.SetActive(true);
            if (ScreenHandler.current != null && CommishSelectScreen != null)
                ScreenHandler.current.OpenPanel(CommishSelectScreen);
            return;
        }

        //StartMyFed();

        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
    }

    public void StartGame()
    {
        if (GamePlaySession.instance.isNewPlayer && !GamePlaySession.instance.newGamePlus)
        {
            // If new player, head to the welcome screen and begin the tutorial!
            if (ScreenHandler.current != null && WelcomeScreen != null)
                ScreenHandler.current.OpenPanel(WelcomeScreen);

            GamePlaySession.instance.myCash = 10000;
            GamePlaySession.instance.myTokens = 1;
            GamePlaySession.instance.isNewPlayer = false;
        }
        else
        {
            PlayerHUD.SetActive(true);
            if (ScreenHandler.current != null && FedScreen != null)
                ScreenHandler.current.OpenPanel(FedScreen);
        }

        PlayerHUD.GetComponent<HUDScript>().SetFedImage();
        PlayerHUD.GetComponent<HUDScript>().SetCommishImage();
        PlayerHUD.GetComponent<HUDScript>().UpdateHUD();
    }

    public void ShowHUD()
    {
        PlayerHUD.SetActive(true);
        UpdateHUD();
    }

    public void UpdateHUD()
    {
        PlayerHUD.BroadcastMessage("ShowMyCash");
        PlayerHUD.BroadcastMessage("ShowMyTokens");
    }

    public void VerifyFed()
    {
        if (ScreenHandler.current != null && FedScreenVerify != null)
            ScreenHandler.current.OpenPanel(FedScreenVerify);
    }
    public void VerifyCommish()
    {
        if (ScreenHandler.current != null && CommishScreenVerify != null)
            ScreenHandler.current.OpenPanel(CommishScreenVerify);
    }

    public void StartMyFed()
    {
        // Add any code needed when first creating the player's federation here.

        if (MyFedScreen == null)
            return;
        OnSaveAll();
        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
    }

    public void GoToMyFed()
    {
        // Should do a check to see if we can go to MyFed screen from current place in game...
        Animator currentPanel = null;
        if (ScreenHandler.current != null)
            currentPanel = ScreenHandler.current.GetCurrentPanel();

        if (currentPanel != MainCalendarScreen)
            return;

        if (MyFedScreen == null)
            return;
        OnSaveAll();
        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
    }

    public void HomeButton()
    {
        if (MyFedScreen == null)
            return;
        OnSaveAll();
        MyFed myFed = MyFedScreen.GetComponent<MyFed>();
        myFed.OpenMyFedPanel();
    }

    public void GoToCalendar()
    {
        if (MainCalendarScreen == null)
            return;
        OnSaveAll();
        CalendarScreen calScreen = MainCalendarScreen.GetComponent<CalendarScreen>();
        calScreen.OpenCalendar();

        if (!GamePlaySession.instance.calendarTutorialClear && !GamePlaySession.instance.newGamePlus)
        {
            GamePlaySession.instance.calendarTutorialClear = true;

            PopUpWindow popUpWindow = GameObject.Find("UIManager").GetComponent<UIManagerScript>().popUpWindow;

            if (popUpWindow != null)
            {
                popUpWindow.OpenPopUpWindow(new PopUpMessage[] { calScreen.initialPopUp }, MainCalendarScreen);
            }
        }
    }

    public void GoToShop()
    {
        if (GamePlaySession.instance == null)
            return;

        if (ShopScreen != null)
        {
            OnSaveAll();
            StorePage storePage = ShopScreen.gameObject.GetComponent<StorePage>();

            if (storePage != null)
                storePage.RefreshCardDisplay();

            if (ScreenHandler.current != null)
            {
                PlayerHUD.SetActive(false);
                ScreenHandler.current.OpenPanel(ShopScreen);
            }
        }
    }

    public void GoToTitle()
    {
        if (ScreenHandler.current != null)
        {
            OnSaveAll();
            PlayerHUD.SetActive(false);
            ScreenHandler.current.OpenPanel(TitleScreen);
        }
    }

    public void GoToCardView()
    {
        if (cardViewer == null)
            return;
        OnSaveAll();
        cardViewer.OpenCardView();
    }

    public void Start()
    {
        if (PlayerPrefs.GetString("GAMEPLAYSESSION", "NONE") != "NONE")
        {
            GamePlaySession.instance = GamePlaySession.Load();
            GamePlaySession.instance.GSI = GetComponent<GameSetupItems>();
            PlayerHUD.GetComponent<HUDScript>().SetFedImage();
            PlayerHUD.GetComponent<HUDScript>().SetCommishImage();
            PlayerHUD.GetComponent<HUDScript>().UpdateHUD();

        }
        else
        {
            Debug.Log("Building new Gameplay Session!");
            GamePlaySession.instance = new GamePlaySession();
        }

        CloseAllPanels();
    }

    void OnSaveAll()
    {
        if (GamePlaySession.instance != null && GamePlaySession.instance.myCards.Count != 0)
            GamePlaySession.instance.SaveAll();
    }

    void CloseAllPanels()
    {
        if (TitleScreen != null)
            TitleScreen.gameObject.SetActive(false);
        if (DisclaimerScreen != null)
            DisclaimerScreen.gameObject.SetActive(false);
        if (WelcomeScreen != null)
            WelcomeScreen.gameObject.SetActive(false);
        if (FedScreen != null)
            FedScreen.gameObject.SetActive(false);
        if (FedScreenVerify != null)
            FedScreenVerify.gameObject.SetActive(false);
        if (MyFedScreen != null)
            MyFedScreen.gameObject.SetActive(false);
        if (CommishSelectScreen != null)
            CommishSelectScreen.gameObject.SetActive(false);
        if (CommishScreenVerify != null)
            CommishScreenVerify.gameObject.SetActive(false);
        if (MainCalendarScreen != null)
            MainCalendarScreen.gameObject.SetActive(false);
        if (NowYouKnowScreen != null)
            NowYouKnowScreen.gameObject.SetActive(false);
        if (ShowSetupScreen != null)
            ShowSetupScreen.gameObject.SetActive(false);
        if (MatchSetupScreen != null)
            MatchSetupScreen.gameObject.SetActive(false);
        if (TagMatchSetupScreen != null)
            TagMatchSetupScreen.gameObject.SetActive(false);
        if (MicSpotSetupScreen != null)
            MicSpotSetupScreen.gameObject.SetActive(false);
        if (SkitSetupScreen != null)
            SkitSetupScreen.gameObject.SetActive(false);
        if (ShowTransitionScreen != null)
            ShowTransitionScreen.gameObject.SetActive(false);
        if (ShowResultsScreen != null)
            ShowResultsScreen.gameObject.SetActive(false);
        if (ShopScreen != null)
            ShopScreen.gameObject.SetActive(false);
        if (HelpScreen != null)
            HelpScreen.gameObject.SetActive(false);
        if (CreditsScreen != null)
            CreditsScreen.gameObject.SetActive(false);
    }
}
