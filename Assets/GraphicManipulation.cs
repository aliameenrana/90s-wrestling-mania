﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class GraphicManipulation : MonoBehaviour {

	public void SetImageSprite(Sprite spr){
		this.GetComponent<Image>().sprite = spr;
	}
}
