﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

[RequireComponent(typeof(AudioSource))]

public class GameSetupItems : MonoBehaviour
{

    public FedComItem[] Federations;
    public FedComItem[] Commisioners;
    public ShowObject[] ShowTypes;

    public WrestlerCardCollection wrestlers;
    public VenueCardCollection venues;
    public FlavorCardCollection flavors;
    public MatchCardCollection matches;
    public MicSpotCardCollection micSpots;
    public SkitCardCollection skits;
    public NYKCardCollection nowyouknows;

    public string wrestlerDoc;
    public string venuesDoc;
    public string flavorsDoc;
    public string matchesDoc;
    public string micSpotsDoc;
    public string skitsDoc;
    public string nykDoc;

    public StorePage storePage;

    public Sprite[] cardFronts;
    public Sprite[] cardLogos;
    public Sprite[] cardHeadshots;
    public AudioClip[] cardAudio;

    public Dictionary<int, Sprite> cardFrontDict;
    public Dictionary<int, Sprite> cardHeadshotDict;
    public Dictionary<int, Sprite> cardLogoDict;
    public Dictionary<int, AudioClip> cardAudioDict;

    void Awake()
    {
        System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
    }

    private IEnumerator ReadEditorType(string xml, Type type)
    {
        WWW www = new WWW("http://red-clove.com.ua/90wsEditor/" + xml);

        yield return www;

        if (www.error != null)
        {
            Debug.Log(www.error);
            yield break;
        }

        if (type == typeof(WrestlerCardCollection))
            wrestlerDoc = www.text;
        else if (type == typeof(VenueCardCollection))
            venuesDoc = www.text;
        else if (type == typeof(FlavorCardCollection))
            flavorsDoc = www.text;
        else if (type == typeof(MatchCardCollection))
            matchesDoc = www.text;
        else if (type == typeof(MicSpotCardCollection))
            micSpotsDoc = www.text;
        else if (type == typeof(SkitCardCollection))
            skitsDoc = www.text;
        else if (type == typeof(NYKCardCollection))
            nykDoc = www.text;

        www.Dispose();
    }

    IEnumerator Start()
    {
        yield return StartCoroutine(ReadEditorType("wrestler.xml", typeof(WrestlerCardCollection)));
        yield return StartCoroutine(ReadEditorType("venues.xml", typeof(VenueCardCollection)));
        yield return StartCoroutine(ReadEditorType("flavors.xml", typeof(FlavorCardCollection)));
        yield return StartCoroutine(ReadEditorType("matches.xml", typeof(MatchCardCollection)));
        yield return StartCoroutine(ReadEditorType("micspots.xml", typeof(MicSpotCardCollection)));
        yield return StartCoroutine(ReadEditorType("skits.xml", typeof(SkitCardCollection)));
        yield return StartCoroutine(ReadEditorType("nowyouknow.xml", typeof(NYKCardCollection)));

        cardFrontDict = new Dictionary<int, Sprite>();
        foreach (Sprite spr in cardFronts)
        {
            cardFrontDict.Add(int.Parse(Regex.Replace(spr.name, "[^0-9]", "")), spr);
        }
        cardHeadshotDict = new Dictionary<int, Sprite>();
        foreach (Sprite spr in cardHeadshots)
        {
            cardHeadshotDict.Add(int.Parse(Regex.Replace(spr.name, "[^0-9]", "")), spr);
        }
        cardLogoDict = new Dictionary<int, Sprite>();
        foreach (Sprite spr in cardLogos)
        {
            cardLogoDict.Add(int.Parse(Regex.Replace(spr.name, "[^0-9]", "")), spr);
        }
        cardAudioDict = new Dictionary<int, AudioClip>();
        foreach (AudioClip aud in cardAudio)
        {
            cardAudioDict.Add(int.Parse(Regex.Replace(aud.name, "[^0-9]", "")), aud);
        }

        wrestlers = (WrestlerCardCollection)WrestlerCardCollection.Load(wrestlerDoc);

        venues = (VenueCardCollection)VenueCardCollection.Load(venuesDoc);

        flavors = (FlavorCardCollection)FlavorCardCollection.Load(flavorsDoc);

        matches = (MatchCardCollection)MatchCardCollection.Load(matchesDoc);
        micSpots = (MicSpotCardCollection)MicSpotCardCollection.Load(micSpotsDoc);

        skits = (SkitCardCollection)SkitCardCollection.Load(skitsDoc);

        nowyouknows = (NYKCardCollection)NYKCardCollection.Load(nykDoc);

        for (int i = 0; i < GamePlaySession.instance.myCards.Count; i++)
        {
            if (GamePlaySession.instance.myCards[i].myCardType == CardType.WRESTLER)
            {
                GamePlaySession.instance.myCards[i] = GetCard(GamePlaySession.instance.myCards[i].CardNumber).CloneWithProgress(GamePlaySession.instance.myCards[i]);
            }
            else
            {
                GamePlaySession.instance.myCards[i] = GetCard(GamePlaySession.instance.myCards[i].CardNumber).Clone();
            }
        }
        foreach (ShowObject so in GamePlaySession.instance.myShows)
        {
            so.PostDeserialize();
        }

        if (storePage != null)
        {
            storePage.CheckCardLibrary();
        }
    }

    public CardObject GetCard(int id)
    {
        Debug.Log(id);

        for (int i = 0; i < wrestlers.cardArray.Length; i++)
        {
            if (wrestlers.cardArray[i].CardNumber == id)
            {
                return wrestlers.cards[id];
            }
        }

        for (int i = 0; i < venues.cardArray.Length; i++)
        {
            if (venues.cardArray[i].CardNumber == id)
            {
                return venues.cards[id];
            }
        }

        for (int i = 0; i < flavors.cardArray.Length; i++)
        {
            if (flavors.cardArray[i].CardNumber == id)
            {
                return flavors.cards[id];
            }
        }

        for (int i = 0; i < matches.cardArray.Length; i++)
        {
            if (matches.cardArray[i].CardNumber == id)
            {
                return matches.cards[id];
            }
        }

        for (int i = 0; i < micSpots.cardArray.Length; i++)
        {
            if (micSpots.cardArray[i].CardNumber == id)
            {
                return micSpots.cards[id];
            }
        }

        for (int i = 0; i < skits.cardArray.Length; i++)
        {
            if (skits.cardArray[i].CardNumber == id)
            {
                return skits.cards[id];
            }
        }

        return null;
    }

    public void PlayTheme()
    {
        GetComponent<AudioSource>().Play();
    }

}

[System.Serializable]
public class CardSpriteEntry
{
    public int CardNumber;
    public Sprite sprite;
}