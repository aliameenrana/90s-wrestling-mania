﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FlashText : MonoBehaviour {

	public bool waiting = true;
	public float timer = .65f;
	// Use this for initialization
	void Start () {
		StartCoroutine(Flash());
	}
	
	// Update is called once per frame
	IEnumerator Flash(){
		while(waiting){

			this.GetComponent<Text>().enabled = true;
			yield return new WaitForSeconds (timer);

			if(!waiting){break;}
			this.GetComponent<Text>().enabled = false;
			yield return new WaitForSeconds (timer);


		}
		this.GetComponent<Text>().enabled = true;
	}
}
